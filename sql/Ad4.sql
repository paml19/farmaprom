select su.name
from system_user su
join system_user_has_attribute suha on su.system_user_id = suha.system_user_id and suha.attribute_id in (1, 2, 4)
group by su.name
having count(suha.attribute_id) = 3
