<?php

namespace Aquarium\Entity;

/**
 * Class Heater
 * @package Aquarium\Entity
 */
class Heater
{
    /**
     *
     */
    const MODE_0 = 0;

    /**
     *
     */
    const MODE_1 = 1;

    /**
     *
     */
    const MODE_2 = 2;

    /**
     * @var int
     */
    private $mode;

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     * @return Heater
     */
    public function setMode(int $mode): Heater
    {
        $this->mode = $mode;
        return $this;
    }
}
