<?php

namespace Aquarium\Entity\LivingThings;

/**
 * Interface SwimmingThingInterface
 * @package Aquarium\Entity
 */
interface SwimmingThingInterface extends LivingThingInterface
{
    /**
     * @return bool
     */
    public function isSwim(): bool;

    /**
     * @param bool $swim
     * @return $this
     */
    public function setSwim(bool $swim): self;

    /**
     * @return string
     */
    public function getSwimWay(): string;

    /**
     * @return bool
     */
    public function isHungry(): bool;

    /**
     * @param bool $hungry
     * @return $this
     */
    public function setHungry(bool $hungry): self;

    /**
     * @return int
     */
    public function getFoodType(): int;
}
