<?php

namespace Aquarium\Entity\LivingThings;

/**
 * Interface LivingThingInterface
 * @package Aquarium\Entity
 */
interface LivingThingInterface
{
    /**
     * @return bool
     */
    public function isCanBreath(): bool;

    /**
     * @param bool $canBreath
     * @return $this
     */
    public function setCanBreath(bool $canBreath): self;
}
