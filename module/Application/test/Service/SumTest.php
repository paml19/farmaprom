<?php

namespace ApplicationTest\Service;

use Application\Service\Sum;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SumTest extends TestCase
{
    private $sum;

    protected function setUp()
    {
        $this->sum = new Sum();
        parent::setUp();
    }

    /**
     * @dataProvider providerException
     */
    public function testSumMethodThrowsException($a, $b, $exception)
    {
        $this->expectException($exception);

        $this->sum->sum($a, $b);
    }

    /**
     * @dataProvider providerReturns
     */
    public function testSumMethodReturnsInt($a, $b, $resultProvider)
    {
        $result = $this->sum->sum($a, $b);

        $this->assertIsInt($result);
        $this->assertEquals($resultProvider, $result);
    }

    public function providerException()
    {
        return [
            ['non-int-value', 'non-int-value', InvalidArgumentException::class],
            ['non-int-value', 87, InvalidArgumentException::class],
            [94, 36.6, InvalidArgumentException::class],
            [null, 25, InvalidArgumentException::class],
            [78, null, InvalidArgumentException::class]
        ];
    }

    public function providerReturns()
    {
        return [
            [0, 0, 0],
            [11, 42, 53],
        ];
    }
}