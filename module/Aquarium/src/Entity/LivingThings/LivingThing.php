<?php

namespace Aquarium\Entity\LivingThings;

/**
 * Class LivingThing
 * @package Aquarium\Entity
 */
abstract class LivingThing
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $canBreath;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return LivingThing
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCanBreath(): bool
    {
        return $this->canBreath;
    }

    /**
     * @param bool $canBreath
     * @return LivingThing
     */
    public function setCanBreath(bool $canBreath): self
    {
        $this->canBreath = $canBreath;
        return $this;
    }
}
