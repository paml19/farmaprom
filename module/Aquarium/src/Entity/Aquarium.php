<?php

namespace Aquarium\Entity;

use Aquarium\Entity\Filter\Filter;
use Aquarium\Entity\LivingThings\LivingThingInterface;

class Aquarium
{
    /**
     * @var array
     */
    private $livingThings;

    /**
     * @var Heater
     */
    private $heater;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var bool
     */
    private $light;

    public function __construct(Heater $heater)
    {
        $this->heater = $heater;
        $this->livingThings = [];
    }

    /**
     * @return array
     */
    public function getLivingThings(): array
    {
        return $this->livingThings;
    }

    public function addLivingThing(LivingThingInterface $livingThing): Aquarium
    {
        $this->livingThings[] = $livingThing;
        return $this;
    }

    /**
     * @return Heater
     */
    public function getHeater(): Heater
    {
        return $this->heater;
    }

    /**
     * @return Filter
     */
    public function getFilter(): Filter
    {
        return $this->filter;
    }

    /**
     * @param Filter $filter
     * @return Aquarium
     */
    public function setFilter(Filter $filter): Aquarium
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLight(): bool
    {
        return $this->light;
    }

    /**
     * @param bool $light
     * @return Aquarium
     */
    public function setLight(bool $light): Aquarium
    {
        $this->light = $light;
        return $this;
    }
}
