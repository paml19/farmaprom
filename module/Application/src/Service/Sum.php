<?php

namespace Application\Service;

use InvalidArgumentException;

/**
 * Class Sum
 * @package Application\Service
 */
class Sum
{
    /**
     * @param $a
     * @param $b
     * @return int
     */
    public function sum($a, $b)
    {
        if (! is_int($a) || ! is_int($b)) {
            throw new InvalidArgumentException();
        }

        return $a + $b;
    }
}