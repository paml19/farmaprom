<?php

namespace Aquarium\Entity\LivingThings;

/**
 * Class Turtle
 * @package Aquarium\Entity
 */
class Turtle extends SwimmingThing implements SwimmingThingInterface
{
    /**
     *
     */
    const TURTLE_FOOD = 1;

    /**
     *
     */
    const SWIM_WAY = 'left';

    /**
     * Turtle constructor.
     */
    public function __construct()
    {
        $this->foodType = self::TURTLE_FOOD;
        $this->swimWay = self::SWIM_WAY;
    }
}
