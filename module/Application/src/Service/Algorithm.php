<?php

namespace Application\Service;

class Algorithm
{
    public function algorithm(array $input): int
    {
        $quantity = 0;
        $items = count($input);

        for ($i = 0; $i < $items; $i++) {
            for ($j = $i + 1; $j < $items; $j++) {
                if ($input[$i] + $input[$j] == 0 && ($input[$i] != 0 && $input[$j] != 0)) {
                    $input[$i] = 0;
                    $input[$j] = 0;
                    $quantity++;
                }
            }
        }

        return $quantity;
    }
}