# All answers are stored in this project

To install project run `composer install`.

## Ad1
ApplicationTest\Service\SumTest

After `composer install` command, run `composer test`.

## Ad2
Application\Service\Algorithm
Application\Controller\IndexController

After `composer install` command, run `php public/index.php run-algorithm`.

## Ad3
Aquarium - all module

## Ad4
./sql/Ad4.sql

## Ad5
./sql/Ad5.sql