<?php

namespace Application;

use Zend\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'console' => [
        'router' => [
            'routes' => [
                'run-algorithm' => [
                    'options' => [
                        'route'    => 'run-algorithm',
                        'defaults' => [
                            'controller' => Controller\IndexController::class,
                            'action'     => 'algorithm',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => ReflectionBasedAbstractFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\Algorithm::class => InvokableFactory::class,
        ],
    ],
];
