<?php

namespace Aquarium;

/**
 * Class Module
 * @package Aquarium
 */
class Module
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
