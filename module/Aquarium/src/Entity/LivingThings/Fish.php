<?php

namespace Aquarium\Entity\LivingThings;

/**
 * Class Fish
 * @package Aquarium\Entity
 */
class Fish extends SwimmingThing implements SwimmingThingInterface
{
    /**
     *
     */
    const FISH_FOOD = 0;

    /**
     *
     */
    const SWIM_WAY = 'right';

    /**
     * Fish constructor.
     */
    public function __construct()
    {
        $this->foodType = self::FISH_FOOD;
        $this->swimWay = self::SWIM_WAY;
    }
}
