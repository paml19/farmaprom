<?php

namespace Aquarium\Service;

use Aquarium\Entity\Aquarium;
use Aquarium\Entity\LivingThings\SwimmingThingInterface;

/**
 * Class AquariumAnimalManager
 * @package Aquarium\Service
 */
class AquariumAnimalManager extends AquariumLivingThingManager
{
    /**
     * @param int $foodType
     * @param Aquarium $aquarium
     * @return void
     */
    public function feedAnimals(int $foodType, Aquarium $aquarium): void
    {
        $animals = $aquarium->getLivingThings();

        /** @var SwimmingThingInterface $animal */
        foreach ($animals as $animal) {
            if ($animal->isHungry() && $animal->getFoodType() == $foodType && $animal instanceof SwimmingThingInterface) {
                $animal->setHungry(false);
            }
        }
    }

    /**
     * @param bool $lightOn
     * @param Aquarium $aquarium
     * @return void
     */
    public function changeLightState(bool $lightOn, Aquarium $aquarium): void
    {
        $aquarium->setLight($lightOn);
        $animals = $aquarium->getLivingThings();

        /** @var SwimmingThingInterface $animal */
        foreach ($animals as $animal) {
            if ($animal instanceof SwimmingThingInterface) {
                $animal->setSwim($lightOn);
            }
        }
    }
}
