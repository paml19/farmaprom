select b.title, count(r.rent_id) as rents
from book b
join rent r on b.book_id = r.book_id
where YEAR(r.date) = 2008
group by b.title