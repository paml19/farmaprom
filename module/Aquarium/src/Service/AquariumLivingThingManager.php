<?php

namespace Aquarium\Service;

use Aquarium\Entity\Aquarium;
use Aquarium\Entity\LivingThings\LivingThingInterface;

/**
 * Class AquariumLivingThingManager
 * @package Aquarium\Service
 */
class AquariumLivingThingManager
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * AquariumLivingThingManager constructor.
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @param LivingThingInterface $livingThing
     * @param Aquarium $aquarium
     * @return void
     */
    public function addOneToAquarium(LivingThingInterface $livingThing, Aquarium $aquarium): void
    {
//        save $livingThing

        $aquarium->addLivingThing($livingThing);

        $this->notificationService->sendSMS();
        $this->notificationService->sendEmail();
    }

    /**
     * @param \Iterator $collection
     * @param Aquarium $aquarium
     * @return void
     */
    public function addMultipleToAquarium(\Iterator $collection, Aquarium $aquarium): void
    {
        foreach ($collection as $item) {
            $this->addOneToAquarium($item, $aquarium);
        }
    }
}
