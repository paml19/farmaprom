<?php

namespace Aquarium\Entity\LivingThings;

/**
 * Class SwimmingThing
 * @package Aquarium\Entity
 */
abstract class SwimmingThing extends LivingThing
{
    /**
     * @var bool
     */
    private $swim;

    /**
     * @var string
     */
    protected $swimWay;

    /**
     * @var bool
     */
    private $hungry;

    /**
     * @var integer
     */
    protected $foodType;

    /**
     * @return bool
     */
    public function isSwim(): bool
    {
        return $this->swim;
    }

    /**
     * @param bool $swim
     * @return self
     */
    public function setSwim(bool $swim): self
    {
        $this->swim = $swim;
        return $this;
    }

    /**
     * @return string
     */
    public function getSwimWay(): string
    {
        return $this->swimWay;
    }

    /**
     * @return bool
     */
    public function isHungry(): bool
    {
        return $this->hungry;
    }

    /**
     * @param bool $hungry
     * @return self
     */
    public function setHungry(bool $hungry): self
    {
        $this->hungry = $hungry;
        return $this;
    }

    /**
     * @return int
     */
    public function getFoodType(): int
    {
        return $this->foodType;
    }
}
