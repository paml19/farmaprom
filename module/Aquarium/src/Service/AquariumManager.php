<?php

namespace Aquarium\Service;

use Aquarium\Entity\Aquarium;

/**
 * Class AquariumManager
 * @package Aquarium\Service
 */
class AquariumManager
{
    /**
     * @param int $mode
     * @param Aquarium $aquarium
     * @return void
     */
    public function setHeaterMode(int $mode, Aquarium $aquarium): void
    {
        $heater = $aquarium->getHeater();
        $heater->setMode($mode);
    }
}
