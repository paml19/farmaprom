<?php

namespace Application\Controller;

use Application\Service\Algorithm;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

/**
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractConsoleController
{
    /**
     * @var Algorithm
     */
    private $algorithm;

    /**
     * IndexController constructor.
     * @param Algorithm $algorithm
     */
    public function __construct(Algorithm $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     *
     */
    public function algorithmAction()
    {
        $input = [3, 6, -3, 5, -10, 3, 10, 1, 7, -1, -9, -8, 7, 7, -7, -2, -7];

//        for ($i = -10; $i <= 10; $i++) {
//            if ($i != 0) {
//                $input[] = $i;
//            }
//        }

        $output = $this->algorithm->algorithm($input);

        $this->getConsole()->writeLine('The quantity equals: ' . $output);
    }
}